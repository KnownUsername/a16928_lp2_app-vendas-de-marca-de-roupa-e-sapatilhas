var searchData=
[
  ['data_20source_2ecsproj_2efilelistabsolute_2etxt_59',['Data Source.csproj.FileListAbsolute.txt',['../_data_01_source_8csproj_8_file_list_absolute_8txt.html',1,'']]],
  ['datacompra_60',['dataCompra',['../class_data_source_1_1_registo_compra.html#aa3dc7881add8de22da14df4197a15429',1,'DataSource::RegistoCompra']]],
  ['datalancamento_61',['dataLancamento',['../class_business_objects_1_1_artigo_b_o.html#a6bae7d149ac4d13473d4f5dd50a8dbef',1,'BusinessObjects.ArtigoBO.dataLancamento()'],['../class_business_objects_1_1_colecao_b_o.html#a45be376323df4e9edcdf096b4eb00dc5',1,'BusinessObjects.ColecaoBO.dataLancamento()'],['../class_business_objects_1_1_artigo_b_o.html#a3ca0837812fc9de5f53ac8ad6c4cb5f1',1,'BusinessObjects.ArtigoBO.DataLancamento()'],['../class_business_objects_1_1_colecao_b_o.html#a9e262e86e4d31b302e94daef5e481329',1,'BusinessObjects.ColecaoBO.DataLancamento()']]],
  ['datanascimento_62',['DataNascimento',['../class_business_objects_1_1_cliente_b_o.html#a8e90bd285fe904125c2b226075373300',1,'BusinessObjects.ClienteBO.DataNascimento()'],['../class_business_objects_1_1_cliente_b_o.html#acdd0f68cccd8da2f72cd781cc6682fa5',1,'BusinessObjects.ClienteBO.dataNascimento()']]],
  ['datasource_63',['DataSource',['../namespace_data_source.html',1,'']]],
  ['datasource_2ecsproj_2efilelistabsolute_2etxt_64',['DataSource.csproj.FileListAbsolute.txt',['../_data_source_8csproj_8_file_list_absolute_8txt.html',1,'']]],
  ['decres_65',['Decres',['../namespace_data_source.html#a5b4a7aebcf32a9100586fb5905d5e1a6a7a4428f13c6e34470b735fd0ac24ba4a',1,'DataSource']]],
  ['descontolog_66',['descontoLog',['../class_data_source_1_1_carrinho.html#ad1230cccfd8774a621fc573ed8e10f0a',1,'DataSource::Carrinho']]],
  ['descricao_67',['Descricao',['../class_business_objects_1_1_artigo_b_o.html#ac4c0095712964ad120d083b4bdc64c5a',1,'BusinessObjects.ArtigoBO.Descricao()'],['../class_business_objects_1_1_artigo_b_o.html#ac7f5b406573c4066dfda0c77eecc7812',1,'BusinessObjects.ArtigoBO.descricao()']]],
  ['disponibilizainfocliente_68',['DisponibilizaInfoCliente',['../class_acessos_1_1_acesso_clientes.html#aff02fbe6f3e4a4dd74b83bf9b0b7441d',1,'Acessos.AcessoClientes.DisponibilizaInfoCliente()'],['../class_data_source_1_1_clientes.html#a92f04755075282e524ae1848991a1a30',1,'DataSource.Clientes.DisponibilizaInfoCliente()']]]
];
