var searchData=
[
  ['idcliente_79',['IdCliente',['../class_data_source_1_1_registo_compra.html#a9b0473755f9cb54ff1ffd4d8a61248dd',1,'DataSource.RegistoCompra.IdCliente()'],['../class_data_source_1_1_registo_compra.html#a1fc2024a35cdd0af71c5141895b9c898',1,'DataSource.RegistoCompra.idCliente()']]],
  ['inicial_80',['Inicial',['../class_interface_empresarial_1_1_menu.html#a81bf09d028c1877cac65d410cf1fe2a6',1,'InterfaceEmpresarial.Menu.Inicial()'],['../class_main_1_1_pagina.html#aabf001d3401c1a5df5a18378bb959742',1,'Main.Pagina.Inicial()']]],
  ['init_81',['Init',['../class_data_source_1_1_carrinho.html#ab7b2709cf250be57f062eb509c5fb92f',1,'DataSource::Carrinho']]],
  ['insereartigo_82',['InsereArtigo',['../class_acessos_1_1_manipulacao_artigos.html#a77a4acb525b3a776b2a9d76e3baafc90',1,'Acessos.ManipulacaoArtigos.InsereArtigo()'],['../class_data_source_1_1_artigos.html#a0e446d535a5fbe41fbfc82bf8f588027',1,'DataSource.Artigos.InsereArtigo()']]],
  ['inserecliente_83',['InsereCliente',['../class_acessos_1_1_acesso_clientes.html#a28ce6adb3ff379768ec923f6006c44f6',1,'Acessos.AcessoClientes.InsereCliente()'],['../class_data_source_1_1_clientes.html#a67c649b03a64710446781af0ee879a7a',1,'DataSource.Clientes.InsereCliente()']]],
  ['inserecor_84',['InsereCor',['../class_data_source_1_1_artigo.html#a9655da39eb15b3b4d462faab8a3aab20',1,'DataSource::Artigo']]],
  ['inseriravaliacao_85',['InserirAvaliacao',['../class_data_source_1_1_artigo.html#aa53a043ce74aabba4bea40d1ffb34247',1,'DataSource::Artigo']]],
  ['interfaceempresarial_86',['InterfaceEmpresarial',['../namespace_interface_empresarial.html',1,'']]],
  ['interfaceempresarial_2ecsproj_2efilelistabsolute_2etxt_87',['InterfaceEmpresarial.csproj.FileListAbsolute.txt',['../_interface_empresarial_8csproj_8_file_list_absolute_8txt.html',1,'']]],
  ['iswithin_88',['IsWithin',['../class_acessos_1_1_validacoes_artigo.html#a9f45a6894f3a5238c0dd0bfc49e7d086',1,'Acessos::ValidacoesArtigo']]]
];
