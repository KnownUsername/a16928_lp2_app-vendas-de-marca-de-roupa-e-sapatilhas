var searchData=
[
  ['validacoesartigo_147',['ValidacoesArtigo',['../class_acessos_1_1_validacoes_artigo.html',1,'Acessos']]],
  ['validacoesartigo_2ecs_148',['ValidacoesArtigo.cs',['../_validacoes_artigo_8cs.html',1,'']]],
  ['validacoescliente_149',['ValidacoesCliente',['../class_acessos_1_1_validacoes_cliente.html',1,'Acessos']]],
  ['validacoescliente_2ecs_150',['ValidacoesCliente.cs',['../_validacoes_cliente_8cs.html',1,'']]],
  ['verde_151',['Verde',['../namespace_business_objects.html#abfa9e0d9c7eb031530d082ecb6d5d6f2a14e6261890e07b8ec2c28ada3961b427',1,'BusinessObjects']]],
  ['verificadatanascimento_152',['VerificaDataNascimento',['../class_acessos_1_1_validacoes_cliente.html#a82dc2862c0d0fcb0cff5c261b06cd13a',1,'Acessos::ValidacoesCliente']]],
  ['verificaemail_153',['VerificaEmail',['../class_acessos_1_1_validacoes_cliente.html#aed50c82cee39172b6503f78cccbbfb0b',1,'Acessos::ValidacoesCliente']]],
  ['verificanome_154',['VerificaNome',['../class_acessos_1_1_validacoes_cliente.html#a3aba63108660c67420911c29f950b568',1,'Acessos::ValidacoesCliente']]],
  ['verificapassword_155',['VerificaPassword',['../class_acessos_1_1_validacoes_cliente.html#a673b4ba6d1f3c40f40abbf4348d381da',1,'Acessos::ValidacoesCliente']]],
  ['verificarating_156',['VerificaRating',['../class_business_objects_1_1_avaliacao.html#ab1d8b8f05970538c8ccfc48c9be337d3',1,'BusinessObjects::Avaliacao']]],
  ['verificatamanhoroupa_157',['VerificaTamanhoRoupa',['../class_acessos_1_1_validacoes_artigo.html#a0b9dd4193f344356af986fc087c21bcd',1,'Acessos::ValidacoesArtigo']]],
  ['verificatamanhosapatilhas_158',['VerificaTamanhoSapatilhas',['../class_acessos_1_1_validacoes_artigo.html#a497f79bbc474f6a1dbba3700de485719',1,'Acessos::ValidacoesArtigo']]],
  ['vermelho_159',['Vermelho',['../namespace_business_objects.html#abfa9e0d9c7eb031530d082ecb6d5d6f2ac0311918ceb4948b7714482ce6f4dc8e',1,'BusinessObjects']]]
];
