/* 
 * Nome: João Rodrigues
 * Nº aluno: 16928
 * UC: Linguagem de Programação II
 * Trabalho Prático 
 * Tema: App vendas de marca de roupa e sapatilhas
 */

using Acessos;
using BusinessObjects;
using System;

/// <summary>
/// Responsável pela interface
/// </summary>
namespace Main
{
    class Program
    {
        static void Main(string[] args)
        {
            AcessoClientes.CarregarListaClientes(); // Carregamento de dados de clientes registados

            #region VARIAVEIS_AUX
            ClienteBO clienteAtual = new ClienteBO();
            DateTime dataAux;

            int optMenu = 7;
            bool reiniciar = false;
            string auxInput;
            bool dataValida;
            #endregion

            do
            {
                Console.Clear();

                Pagina.Inicial(clienteAtual);
                
                // Reinicialização do menu, caso a opção submetida não seja inteira 
                if (!Int32.TryParse(Console.ReadLine(), out optMenu))
                {
                    optMenu = -1;
                    continue;
                }

                switch (optMenu)
                {
                    case 0:  break; // Fecha o programa

                    case 1: break;
                    case 2:
                        Pagina.Homem();
                        break;
                    case 3:
                        Pagina.Mulher();
                        break;
                    case 4:
                        Pagina.Crianca();
                        break;
                    case 5:
                        Pagina.Carrinho();
                        break;
                    case 6: // Login / Registo
                        {
                            Console.Clear();
                            Pagina.Login();
                            Int32.TryParse(Console.ReadLine(), out optMenu);
                            switch (optMenu)
                            {
                                case 1: //      Login / Logout
                                    if (clienteAtual.Logged == LOG.OFF) // Login
                                    {
                                        do
                                        {
                                            Console.Clear();

                                            Console.WriteLine("\nCaso queira voltar atrás, introduza 0 num dos campos seguintes");
                                            Console.Write("\n\n Email: ");
                                            auxInput = Console.ReadLine();

                                            #region VERIFICAÇÃO_EMAIL
                                            if (auxInput == null)
                                            {
                                                string errorMessage = ("\nEmail inválido, por favor insira um email válido.");
                                                Pagina.Aviso(errorMessage, ref reiniciar);
                                                continue;
                                            }

                                            if (auxInput == "0") break; // Quando introduzido 0, volta ao menu anterior

                                            if (!AcessoClientes.EmailExiste(auxInput)) // Verifica se o email está registado
                                            {
                                                string errorMessage = ("\n Email não registado. Por favor, certifique-se que introduziu o email correto." +
                                                                 "\n Se ainda não se registou, por favor, proceda ao registo.");
                                                Pagina.Aviso(errorMessage, ref reiniciar);
                                                continue;
                                            }

                                            clienteAtual.Email = auxInput;

                                            #endregion

                                            Console.Write("\n Password: ");
                                            auxInput = Console.ReadLine();

                                            if (auxInput != null) // verificação se input nulo
                                            {
                                                if (auxInput == "0") // Quando introduzido 0, volta ao menu anterior
                                                {
                                                    reiniciar = true;
                                                    break;                                              
                                                }

                                                if (AcessoClientes.PasswordCorresponde(clienteAtual.Email, auxInput)) // verificação se a password introduzida corresponde ao email introduzido
                                                {
                                                    clienteAtual = AcessoClientes.DisponibilizaInfoCliente(clienteAtual.Email); // disponibiliza, ao cliente, as suas informações
                                                    clienteAtual.Logged = LOG.ON; // o estado do cliente passa para logado (= com sessão iniciada)
                                                }
                                            }
                                            else // Submissão inválida
                                            {
                                                string errorMessage = ("\nSenha incorreta!Por favor reveja os seus dados.");
                                                Pagina.Aviso(errorMessage, ref reiniciar);
                                                continue;
                                            }

                                        } while (reiniciar == true);
                                    }

                                    else // Logout
                                    {
                                        do
                                        {
                                            Console.Clear();
                                            Console.WriteLine("Têm a certeza que pretende encerrar a sua sessão? [S/N]");
                                            auxInput = Console.ReadLine();

                                            if ((auxInput == "S") || (auxInput == "s")) // Caso o utilizador confirme querer terminar a sessão
                                            {
                                                clienteAtual.Logged = LOG.OFF; // o estado de login passa para OFF
                                                clienteAtual = null; // é reiniciado o cliente, para um uso próximo
                                            }
                                            else if  (auxInput != "N" && auxInput == "n") reiniciar = true; // caso o input seja diferente de S/s ou N/n, é refeita a pergunta

                                        } while (reiniciar == true);
                                    }
                                    reiniciar = false; // reinicialização da variável para posterior uso
                                    break;

                                case 2: // Registo
                                    do
                                    {
                                        Console.Clear();

                                        #region NOME
                                        Console.Write("\tNome: ");
                                        auxInput = Console.ReadLine();
                                        if (auxInput != null) // caso o input não seja nulo, analia-se o input introduzido
                                        {
                                            if (auxInput == "0") break; // Quando introduzido 0, volta ao menu anterior

                                            if (ValidacoesCliente.VerificaNome(auxInput)) clienteAtual.Nome = auxInput; // Validação do nome introduzido
                                            else // Nome Inválido
                                            {
                                                Pagina.AvisoInvalido("Nome ", ref reiniciar);
                                                continue;
                                            }
                                        }
                                        else // Input Nulo
                                        {
                                            Pagina.AvisoInvalido("Nome ", ref reiniciar);
                                            continue;
                                        }
                                        #endregion

                                        #region APELIDO
                                        Console.Write("\tApelido: ");
                                        auxInput = Console.ReadLine();
                                        if (auxInput != null) // caso o input não seja nulo, analia-se o input introduzido
                                        {
                                            if (auxInput == "0") break; // Quando introduzido 0, volta ao menu anterior

                                            if (ValidacoesCliente.VerificaNome(auxInput)) clienteAtual.Apelido = auxInput; // Validação do nome introduzido
                                            else // Apelido Inválido
                                            {
                                                Pagina.AvisoInvalido("Apelido ", ref reiniciar);
                                                continue;
                                            }
                                        }
                                        else // Input Nulo
                                        {
                                            Pagina.AvisoInvalido("Apelido ", ref reiniciar);
                                            continue;
                                        }
                                        #endregion

                                        #region DATA_NASCIMENTO
                                        Console.Write("\tData de nascimento: ");
                                        auxInput = Console.ReadLine();
                                        if (auxInput == null)  // verificação se é nulo
                                        {
                                            Pagina.AvisoInvalida("Data ", ref reiniciar);
                                            continue;
                                        }
                                        else if (auxInput == "0")  break;// Quando introduzido 0, volta ao menu anterior


                                        dataValida = DateTime.TryParse(auxInput, out dataAux);
                                        if ( (dataValida == true) && (ValidacoesCliente.VerificaDataNascimento(dataAux) )) // Verificação se o input foi convertido com sucesso para DateTime e se é uma data válida
                                        {
                                            clienteAtual.DataNascimento = dataAux;
                                        }
                                        else // Caso a data introduzida não seja válida
                                        {
                                            Pagina.AvisoInvalida("Data ", ref reiniciar);
                                            continue;
                                        }

                                        #endregion
                                        // Pôr sexo ?

                                        #region EMAIL
                                        Console.Write("\tEmail: ");
                                        auxInput = Console.ReadLine();
                                        if ((auxInput != null) && (auxInput != "0") && (ValidacoesCliente.VerificaEmail(auxInput))) // Verificação se input nulo + se input == 0 + se email possível (Interseção de acontecimentos)
                                        {
                                            if (AcessoClientes.EmailExiste(clienteAtual.Email)) // verificação se email já foi registado
                                            {
                                                string errorMessage = "Email já existente! Se já tem conta, por favor inicie sessão!";
                                                Pagina.Aviso(errorMessage, ref reiniciar);
                                                continue;
                                            }
                                            else clienteAtual.Email = auxInput;
                                        }
                                        else if (auxInput == "0") break; // Quando introduzido 0, volta ao menu anterior
                                        else // Email inválido
                                        {
                                            Pagina.AvisoInvalido("Email ", ref reiniciar);
                                            continue;
                                        }
                                        #endregion

                                        #region PASSWORD
                                        Console.Write("\tPassword: ");
                                        auxInput = Console.ReadLine();

                                        if ((auxInput != null) && (auxInput != "0") && (ValidacoesCliente.VerificaPassword(auxInput))) // Verificação se input nulo + se input == 0 + se password possível (Interseção de acontecimentos)
                                        {
                                            clienteAtual.Password = auxInput;
                                        }
                                        else if (auxInput == "0") break; // Quando introduzido 0, volta ao menu anterior
                                        else
                                        {
                                            string errorMessage = "Password inválida! A password deve conter pelo menos 6 caracteres, contendo pelo menos: 1 letra maiúscula, 1 letra minúscula e 1 número.";
                                            Pagina.Aviso(errorMessage, ref reiniciar);
                                            continue;
                                        }
                                        #endregion

                                        clienteAtual.Logged = LOG.ON;
                                        clienteAtual.NumeroCliente = AcessoClientes.InsereCliente(clienteAtual);

                                    } while (reiniciar == true);

                                    break;
                            }
                            break;
                        } 
                    default: continue;
                }
            }
            while (optMenu != 0);

            AcessoClientes.GuardarClientes(); // Guarda clientes em ficheiro
            Environment.Exit(0); // Fecha consola
        }
    }
}
