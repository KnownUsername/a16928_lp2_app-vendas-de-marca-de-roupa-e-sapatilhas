﻿/* 
 * Nome: João Rodrigues
 * Nº aluno: 16928
 * UC: Linguagem de Programação II
 * Trabalho Prático 
 * Tema: App vendas de marca de roupa e sapatilhas
 */

using System;
using BusinessObjects;
using System.Threading;

/// <summary>
/// Responsável pela interface
/// </summary>
namespace Main
{
    /// <summary>
    /// Responsável por lidar com as páginas, a nível de interface, e exposição de erros
    /// </summary>
    public static class Pagina
    {
        public static void Inicial(ClienteBO cl)
        {
            // Opções possíveis 
            Console.WriteLine("\n\t1.Home");
            Console.WriteLine("\t2.Homem");
            Console.WriteLine("\t3.Mulher");
            Console.WriteLine("\t4.Criança");
            Console.WriteLine("\t5.Carrinho");

            // Alternância do texto, conforme o estado de login do utilizador 
            if (cl.Logged == LOG.OFF) Console.WriteLine("\t6.Iniciar sessão / Registo"); // Sem sessão iniciada
            else // com sessão iniciada
            {
                Console.WriteLine("\t6.Encerrar sessão");
                Console.WriteLine("\n\t{0}", cl.Nome);
            }

            Console.WriteLine("\n\t0.Fechar");
            Console.Write("\n Opção: ");
        }

        public static void Homem() { }
        public static void Mulher() { }
        public static void Crianca() { }
        public static void Carrinho() { }

        public static void Login() {
            Console.WriteLine("\n 1.Login ");
            Console.WriteLine(" 2.Registo ");
            Console.Write("\n Opção: ");
        }

        #region AVISOS
        public static void Aviso (string message, ref bool activReiniciar){
            Console.WriteLine(message);
            Thread.Sleep(2000);
            activReiniciar = true;
        }

        public static void AvisoInvalido(string message, ref bool activReiniciar)
        {
            Console.WriteLine(message + "inválido!");
            Thread.Sleep(2000);
            activReiniciar = true;
        }

        public static void AvisoInvalida(string message, ref bool activReiniciar)
        {
            Console.WriteLine(message + "inválida!");
            Thread.Sleep(2000);
            activReiniciar = true;
        }

        #endregion
    }


}
