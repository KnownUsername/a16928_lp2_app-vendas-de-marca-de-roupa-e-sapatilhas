﻿/* 
 * Nome: João Rodrigues
 * Nº aluno: 16928
 * UC: Linguagem de Programação II
 * Trabalho Prático 
 * Tema: App vendas de marca de roupa e sapatilhas
 */

using Acessos;
using BusinessObjects;
using System;

/// <summary>
/// Ações feitas pela empresa
/// </summary>
namespace InterfaceEmpresarial
{
    class Program
    {
        static void Main(string[] args)
        {
            ManipulacaoArtigos.CarregarListaArtigos();

            ArtigoBO artigoAtual = new ArtigoBO();
            string input;
            int optMenu;
            bool reiniciar = false;
            float floatVal;

            do
            {
                Console.Clear();
                Menu.Inicial();
                input = Console.ReadLine();
                if (int.TryParse(input, out optMenu) && (optMenu == 1 || optMenu == 2))
                {
                    switch (optMenu)
                    {
                        case 1: // Inserir artigo
                            do
                            {
                                reiniciar = false;
                                Console.Clear();

                                #region TIPO
                                Menu.Tipos();
                                input = Console.ReadLine();

                                if (int.TryParse(input, out optMenu) && (optMenu >= 1 && optMenu <= 4))
                                {
                                    switch (optMenu) // diferentes tipos de artigos
                                    {
                                        case 1:
                                            artigoAtual.Tipo = TIPO.CALCAS;
                                            break;
                                        case 2:
                                            artigoAtual.Tipo = TIPO.CAMISOLA;
                                            break;
                                        case 3:
                                            artigoAtual.Tipo = TIPO.SWEATSHIRT;
                                            break;
                                        case 4:
                                            artigoAtual.Tipo = TIPO.SAPATILHAS;
                                            break;
                                    }
                                }
                                else
                                {
                                    reiniciar = true;
                                    continue;
                                }
                                #endregion

                                #region MODELO
                                Console.Write("Modelo: ");
                                artigoAtual.Modelo = Console.ReadLine();
                                #endregion

                                #region PRECO
                                Console.Write("Preço: ");
                                input = Console.ReadLine();
                                if (float.TryParse(input, out floatVal)) artigoAtual.Preco = floatVal; // Conversão para float, se possível
                                else
                                {
                                    reiniciar = true;
                                    continue;
                                }
                                #endregion

                                #region SEXO
                                Menu.Sexo();
                                input = Console.ReadLine();
                                if ((int.TryParse(input, out optMenu) && (optMenu >= 1 && optMenu <= 5)))
                                {
                                    switch (optMenu) // diferentes tipos de categorias de idades/géneros
                                    {
                                        case 1:
                                            artigoAtual.Genero = SexoIdade.BEBE;
                                            break;
                                        case 2:
                                            artigoAtual.Genero = SexoIdade.CRIANCA;
                                            break;
                                        case 3:
                                            artigoAtual.Genero = SexoIdade.JOVEM;
                                            break;
                                        case 4:
                                            artigoAtual.Genero = SexoIdade.HOMEM;
                                            break;
                                        case 5:
                                            artigoAtual.Genero = SexoIdade.MULHER;
                                            break;
                                    }
                                }
                                #endregion

                                #region TAMANHO
                                Console.Write("Tamanho: ");
                                input = Console.ReadLine();

                                if (artigoAtual.Tipo == TIPO.SAPATILHAS) // Tamanhos de Sapatilhas
                                {
                                    float.TryParse(input, out floatVal);
                                    if (ValidacoesArtigo.VerificaTamanhoSapatilhas(artigoAtual.Genero, floatVal)) artigoAtual.Tamanho = input; // validação do tamanho submetido 
                                    else
                                    {
                                        reiniciar = true;
                                        continue;
                                    }
                                }
                                else // Tamanhos de Roupa
                                {
                                    if (ValidacoesArtigo.VerificaTamanhoRoupa(input)) artigoAtual.Tamanho = input; // validação do tamanho submetido
                                    else
                                    {
                                        reiniciar = true;
                                        continue;
                                    }
                                }
                                #endregion

                                #region DESCRICAO
                                Console.Write("Descrição: ");
                                artigoAtual.Descricao = Console.ReadLine();
                                #endregion

                                ManipulacaoArtigos.InsereArtigo(artigoAtual);
                            } while (reiniciar == true);
                            break;

                        case 2: // Consultar artigos

                            break;
                    }
                }
            } while (reiniciar == true);
            ManipulacaoArtigos.GuardarListaArtigos();

        }
    }
}
