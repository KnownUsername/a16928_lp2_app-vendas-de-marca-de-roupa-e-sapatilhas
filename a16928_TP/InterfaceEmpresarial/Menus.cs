﻿/* 
 * Nome: João Rodrigues
 * Nº aluno: 16928
 * UC: Linguagem de Programação II
 * Trabalho Prático 
 * Tema: App vendas de marca de roupa e sapatilhas
 */

using System;

/// <summary>
/// Ações feitas pela empresa
/// </summary>
namespace InterfaceEmpresarial
{
    /// <summary>
    /// Responsável por lidar com as páginas, a nível de interface
    /// </summary>
    public static class Menu
    {
        public static void Inicial()
        {
            Console.WriteLine("1. Inserir artigo");
            Console.WriteLine("2. Consultar artigos");
        }

        public static void Tipos()
        {
            Console.WriteLine("Tipo: ");
            Console.WriteLine("\t1. Calças");
            Console.WriteLine("\t2. Camisola");
            Console.WriteLine("\t3. Sweatshirt");
            Console.WriteLine("\t4. Sapatilhas");
            Console.Write("Opção: ");
        }

        public static void Sexo()
        {
            Console.WriteLine("Sexo: ");
            Console.WriteLine("\t1. Bebé");
            Console.WriteLine("\t2. Criança");
            Console.WriteLine("\t3. Jovem");
            Console.WriteLine("\t4. Homem");
            Console.WriteLine("\t5. Mulher");
            Console.Write("Opção: ");
        }
    }
}
