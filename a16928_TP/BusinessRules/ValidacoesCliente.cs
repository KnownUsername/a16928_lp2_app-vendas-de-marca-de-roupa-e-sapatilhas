﻿/* 
 * Nome: João Rodrigues
 * Nº aluno: 16928
 * UC: Linguagem de Programação II
 * Trabalho Prático 
 * Tema: App vendas de marca de roupa e sapatilhas
 */

using System;
using System.Linq;

/// <summary>
/// Estabelece a conexão entre a interface e os dados
/// </summary>
namespace Acessos
{
    /// <summary>
    /// Valida dados introduzidos pelo cliente
    /// </summary>
    public static class ValidacoesCliente
    {
        #region VALIDACOES
        /// <summary>
        /// Verifica se o nome/apelido introduzido
        /// é válido
        /// </summary>
        /// </criter
        /// <param name="nome"></param>
        /// <returns></returns>
        public static bool VerificaNome(string nome)
        {
            if (nome == " ") return false; // caso seja só um espaço
            foreach (char element in nome)
            {
                if (Char.IsDigit(element) || Char.IsSymbol(element)) return false; // não pode conter dígitos
            }
            return true;
        }

        /// <summary>
        /// Verifica datas de nascimento submetidas
        /// </summary>
        /// </standards>
        /// - a data não pode ser superior à do próprio dia
        /// - o ano não pode ser inferior a 1920
        /// </standards>
        /// <param name="data"></param>
        /// <returns></returns>
        public static bool VerificaDataNascimento(DateTime data)
        {
            if (data >= DateTime.Today) return false; // não são aceites datas após o próprio dia
            if (data.Year <= 1920) return false; // caso o ano introduzido seja anterior a 1920

            return true;
        }

        /// <summary>
        /// Verifica se um email é válido
        /// </summary>
        /// </standards>
        /// - Conter "@"
        /// - Conter ".com"
        /// </standards>
        /// <param name="email"></param>
        /// <returns></returns>
        public static bool VerificaEmail(string email)
        {
            if (email.Contains("@") && ((email.Contains(".com") || (email.Contains(".pt"))))) return true; // tem de conter @ e o domínio .com ou .pt
            return false;
        }

        /// <summary>
        /// Verifica se uma password é válida
        /// </summary>
        /// <criterias>
        /// Conter:
        ///     - 1 letra Maiúscula
        ///     - 1 número
        ///     - 1 letra minúscula
        ///     - Tamanho >= 6
        /// </criterias>
        /// <param name="password"></param>
        /// <returns></returns>
        public static bool VerificaPassword(string password)
        {
            // Verificação de critérios
            if ((password.Length > 5) &&
                 password.Any(char.IsUpper) &&  // Maiúscula
                (password.Any(char.IsNumber) && // Número
                (password.Any(char.IsLower)))) return true; // Minúscula

            return false;
        }
        #endregion
    }
}
