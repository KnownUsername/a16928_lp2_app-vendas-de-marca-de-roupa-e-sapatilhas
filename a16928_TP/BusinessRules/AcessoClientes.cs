﻿/* 
 * Nome: João Rodrigues
 * Nº aluno: 16928
 * UC: Linguagem de Programação II
 * Trabalho Prático 
 * Tema: App vendas de marca de roupa e sapatilhas
 */

using BusinessObjects;
using DataSource;

/// <summary>
/// Estabelece a conexão entre a interface e os dados
/// </summary>
namespace Acessos
{
    /// <summary>
    /// Disponibiliza funcionalidades ao utilizador, ao qual este tem permissão
    /// </summary>
    public static class AcessoClientes
    {
        #region MANIPULACAO_DE_DADOS
        /// <summary>
        /// Verifica se o email está registado
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public static bool EmailExiste(string email)
        {
            return Clientes.EmailExiste(email);       
        }

        /// <summary>
        /// Verifica se a password introduzida coincide com a password do email introduzido
        /// </summary>
        /// <param name="email"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public static bool PasswordCorresponde(string email, string password)
        {
            return Clientes.PasswordCorresponde(email, password);
        }

        /// <summary>
        /// Disponibiliza a informação do utilizador, ao mesmo, assim que inicia sessão
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public static ClienteBO DisponibilizaInfoCliente(string email)
        {
           return Clientes.DisponibilizaInfoCliente(email);
        }

        /// <summary>
        /// Adiciona um cliente a uma lista de clientes
        /// </summary>
        /// <param name="novoCliente"></param>
        /// <returns></returns>
        public static int InsereCliente(ClienteBO novoCliente)
        {
            return Clientes.InsereCliente(novoCliente);
        }

        #endregion

        #region FICHEIROS
        /// <summary>
        /// Guarda a lista de todos os clientes
        /// </summary>
        public static void GuardarClientes()
        {
            Clientes.GuardaClientes();
        }

        /// <summary>
        /// Carrega dados da lista de clientes
        /// </summary>
        public static void CarregarListaClientes()
        {
            Clientes.CarregarListaClientes();
        }
        #endregion

        #region PAGAMENTOS
        public static RegistoCompra Pagamento(LOG logStatus, Carrinho carrinho)
        {
            carrinho.SetTotal(logStatus); // calculo de valor Total
            carrinho.Estado = ESTADO.PAGO;
            RegistoCompra registo = new RegistoCompra(carrinho); // criação de carrinho auxiliar

            
            
            return registo;
        }

        #endregion
    }
}
