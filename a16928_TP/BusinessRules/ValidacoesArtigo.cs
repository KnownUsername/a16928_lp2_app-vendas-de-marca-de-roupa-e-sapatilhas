﻿/* 
 * Nome: João Rodrigues
 * Nº aluno: 16928
 * UC: Linguagem de Programação II
 * Trabalho Prático 
 * Tema: App vendas de marca de roupa e sapatilhas
 */

using BusinessObjects;
using System;
using System.Linq;

/// <summary>
/// Estabelece a conexão entre a interface e os dados
/// </summary>
namespace Acessos
{
    /// <summary>
    /// Valida dado introduzidos de um artigo
    /// </summary>
    public static class ValidacoesArtigo
    {
        /// <summary>
        /// Verifica se o tamanho introduzido é válido, tendo em conta o género/ faixa etária
        /// </summary>
        /// <param name="sexoIdade"></param>
        /// <param name="tamanho"></param>
        /// <returns></returns>
        public static bool VerificaTamanhoSapatilhas(SexoIdade sexoIdade, float tamanho) // NOTA: Acrescentar valores de 0.5 em 0.5
        {

            #region CRIANCA
            if (sexoIdade == SexoIdade.BEBE)
                if (IsWithin(tamanho, 17, 27)) return true;

            if (sexoIdade == SexoIdade.CRIANCA)
                if (IsWithin(tamanho, 27.5f, 35)) return true;

            if (sexoIdade == SexoIdade.JOVEM)
                if (IsWithin(tamanho, 35.5f, 40)) return true;

            #endregion

            #region ADULTO
            if (sexoIdade == SexoIdade.HOMEM)
                if (IsWithin(tamanho, 38.5f, 49.5f)) return true;

            if (sexoIdade == SexoIdade.MULHER)
                if (IsWithin(tamanho, 32, 48)) return true;

            #endregion

            return false;
        }

        /// <summary>
        /// Método que verifica se um valor float se encontra num determinado intervalo
        /// </summary>
        /// <param name="value"></param>
        /// <param name="minimum"></param>
        /// <param name="maximum"></param>
        /// <returns></returns>
        public static bool IsWithin(float value, float minimum, float maximum)
        {
            return value >= minimum && value <= maximum;
        }

        /// <summary>
        /// Verifica se o tamanho introduzido é um tamanho de roupa
        /// </summary>
        /// <param name="tamanho"></param>
        /// <returns></returns>
        public static bool VerificaTamanhoRoupa(string tamanho)
        {
            if (Enum.GetNames(typeof(TAMANHO_ROUPAS)).Contains(tamanho)) return true;
            else return false;
        }
    }
}

