﻿/* 
 * Nome: João Rodrigues
 * Nº aluno: 16928
 * UC: Linguagem de Programação II
 * Trabalho Prático 
 * Tema: App vendas de marca de roupa e sapatilhas
 */

using BusinessObjects;
using DataSource;
using System;

/// <summary>
/// Estabelece a conexão entre a interface e os dados
/// </summary>
namespace Acessos
{
    /// <summary>
    /// Disponibiliza funcionalidades de acesso a dados
    /// </summary>
    public static class ManipulacaoArtigos
    {
        /// <summary>
        /// Carrega dados da Lista de Artigos
        /// </summary>
        /// <returns></returns>
        public static bool CarregarListaArtigos()
        {
            try
            {
                if (Artigos.CarregarArtigosJson()) return true;
            } catch (Exception e) { throw e; }
            return false;
        }

        /// <summary>
        /// Guarda dados da Lista de Artigos
        /// </summary>
        /// <returns></returns>
        public static bool GuardarListaArtigos()
        {
            try
            {
                if (Artigos.GuardaArtigosJson()) return true;
            }catch (Exception e) { throw e; }
            return false;
        }

        /// <summary>
        /// Insere 1 artigo na lista de artigos
        /// </summary>
        /// <returns></returns>
        public static bool InsereArtigo(ArtigoBO artigo)
        {
            try
            {
                if (Artigos.InsereArtigo(artigo)) return true;
            }catch (Exception e) { throw e; }

            return false;
        }

        public static bool ConsultarArtigos()
        {
            return true;
        }
            
    }
}
