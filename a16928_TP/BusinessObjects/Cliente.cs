/* 
 * Nome: João Rodrigues
 * Nº aluno: 16928
 * UC: Linguagem de Programação II
 * Trabalho Prático 
 * Tema: App vendas de marca de roupa e sapatilhas
 */

using System;

/// <summary>
/// Definição de Entidades
/// </summary>
namespace BusinessObjects
{
    /// <summary>
    /// Indica se algo têm a sessão iniciada
    /// </summary>
    public enum LOG
    {
        ON,
        OFF
    }

    [Serializable]
    /// <summary>
    /// Define um cliente e as funções a que tem acesso
    /// </summary>
    public class ClienteBO
    {
        string nome, apelido, email, password;
        DateTime dataNascimento;
        int numeroCliente;
        LOG logged;

        #region PROPRIEDADES
        public string Nome
        {
            get { return nome; }
            set { nome = value; }
        }

        public string Apelido
        {
            get { return apelido; }
            set { apelido = value; }
        }

        public DateTime DataNascimento
        {
            get { return dataNascimento; }
            set { dataNascimento = value; }
        }

        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        public string Password
        {
            get { return password; }
            set { password = value; }
        }

        public int NumeroCliente { get; set; }

        public LOG Logged
        {
            get { return logged; }
            set { logged = value; }
        }

        #endregion 

        #region CONSTRUTOR
        /// <summary>
        /// Cria uma instância de Cliente, com sessão não iniciada
        /// </summary>
        public ClienteBO()
        {
            Logged = LOG.OFF; /* Quando é utilizada, uma instância da classe Cliente, apenas fica com o logged on
                                assim que for validada, como verdadeiramente um cliente */
        }

        #endregion

    }
}
