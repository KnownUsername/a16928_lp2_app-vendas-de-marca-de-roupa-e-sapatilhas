/* 
 * Nome: Jo�o Rodrigues
 * N� aluno: 16928
 * UC: Linguagem de Programa��o II
 * Trabalho Pr�tico 
 * Tema: App vendas de marca de roupa e sapatilhas
 */

using System;

/// <summary>
/// Defini��o de Entidades
/// </summary>
namespace BusinessObjects
{
    [Serializable]
    /// <summary>
    /// Cole��o - aglomerado de roupas e/ou sapatilhas
    /// </summary>
    public class ColecaoBO
    {
        string nome;
        DateTime dataLancamento;

        #region PROPRIEDADES

        public string Nome {
            get { return nome; }
            set { nome = value; }
        }

        public DateTime DataLancamento
        {
            get { return dataLancamento; }
            set { dataLancamento = value; }
        }
        #endregion
    }
}   
