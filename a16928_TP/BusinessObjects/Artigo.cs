/* 
 * Nome: João Rodrigues
 * Nº aluno: 16928
 * UC: Linguagem de Programação II
 * Trabalho Prático 
 * Tema: App vendas de marca de roupa e sapatilhas
 */

using System;

/// <summary>
/// Definição de Entidades
/// </summary>
namespace BusinessObjects
{

    [Serializable]
    /// <summary>
    /// Contêm aspectos comuns de roupas e sapatilhas
    /// sendo ambos, artigos de venda
    /// </summary>
    public class ArtigoBO
    {
        string modelo, referencia, descricao, tamanho;
        float preco;
        DateTime dataLancamento;
        SexoIdade genero; // género & idade (adulto/criança)
        TIPO tipo;

        #region PROPRIEDADES
        public string Modelo { get; set; }
        public string Referencia { get; set; }
        public string Descricao { get; set; }
        public string Tamanho
        {
            get { return tamanho; }
            set { tamanho = value; }
        }
        public float Preco
        {
            get { return preco; }
            set { if (value > 0) preco = value; } // não são aceites preços negativos
        }

        public DateTime DataLancamento
        {
            get { return dataLancamento; }
            set { if (value <= DateTime.Today) dataLancamento = value; } // não contando com futuros lançamentos, provisóriamente
        }

        public SexoIdade Genero { get; set; }
        public TIPO Tipo { get; set; }
        #endregion

    }

    #region ENUMERADOS
    public enum SexoIdade
    {
        BEBE,
        CRIANCA,
        JOVEM,
        HOMEM,
        MULHER
    }

    public enum COR
    {
        Amarelo,
        Azul,
        Branco,
        Cinzento,
        Laranja,
        Preto,
        Roxo,
        Verde,
        Vermelho,
    }

    public enum TIPO
    {
        CALCAS,
        CAMISOLA,
        SWEATSHIRT,
        SAPATILHAS
    }

    public enum TAMANHO_ROUPAS // tamanhos possíveis
    {
        XXS,
        XS,
        S,
        M,
        L,
        XL,
        XXL,
        XXXL
    }
    #endregion

    [Serializable]
    /// <summary>
    /// Define a avaliação de um produto
    /// </summary>
    public class Avaliacao
    {
        string comentario;
        int rating;

        // Rating é átribuído, caso o valor pertença a [1,5]
        public int Rating
        {
            get { return rating; }
            set { if (VerificaRating(value)) rating = value; }
        }

        // Função que verifica se o valor introduzido é válido
        public bool VerificaRating(int rating)
        {
            if ((rating > 0) && (rating < 6)) return true;
            return false;
        }

        // Introdução de comentário avaliativo de um produto
        public string Comentario {get; set;}
    }

    
}
