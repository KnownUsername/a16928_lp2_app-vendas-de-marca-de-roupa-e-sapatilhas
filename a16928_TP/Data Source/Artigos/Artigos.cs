﻿/* 
 * Nome: João Rodrigues
 * Nº aluno: 16928
 * UC: Linguagem de Programação II
 * Trabalho Prático 
 * Tema: App vendas de marca de roupa e sapatilhas
 */

using BusinessObjects;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;

/// <summary>
/// Gestor de dados
/// </summary>
namespace DataSource
{
    [Serializable]
    /// <summary>
    /// Classe ArtigoBO + Listas que lhe pertencem
    /// </summary>
    public class Artigo
    {
        ArtigoBO artigoInfos;
        List<COR> cores; // cores que formam o artigo
        List<Avaliacao> avaliacoes;

        #region PROPRIEDADES
        public ArtigoBO ArtigoInfos
        {
            get { return artigoInfos; }
            set { artigoInfos = value; }
        }
        #endregion

        /// <summary>
        /// Insere uma cor na lista, se não for repetida / 
        /// se não tiver sido já submetida
        /// </summary>
        /// <param name="cores"></param>
        /// <param name="cor"></param>
        /// <returns></returns>
        public bool InsereCor(COR cor)
        {
            if (cores.Contains(cor)) return false; // Verifica se a cor já se encontra na lista
            return true;
        }

        /// <summary>
        /// Insere uma avaliação de um produto na sua lista de avaliações
        /// </summary>
        /// <param name="avaliacao"></param>
        /// <returns></returns>
        public bool InserirAvaliacao(Avaliacao avaliacao)
        {
            if (avaliacao != null) // verificação se a avaliação está vazia
            {
                avaliacoes.Add(avaliacao);
                return true;
            }
            return false;
        }
    }

    [Serializable]
    /// <summary>
    /// Armazena artigos à venda
    /// </summary>
    public class Artigos
    {
        static List<Artigo> artigosVenda;
        static string filePath = Environment.CurrentDirectory + "\\artigos.json"; // destino dos dados sobre artigos

        /// <summary>
        /// Adiciona um artigo a uma lista de artigos
        /// </summary>
        /// <param name="novoArtigo"></param>
        /// <returns></returns>
        public static bool InsereArtigo(ArtigoBO novoArtigo)
        {
            try
            {
                Artigo artigoAux = new Artigo();
                if (artigosVenda == null) artigosVenda = new List<Artigo>();

                artigoAux.ArtigoInfos = novoArtigo;
                artigosVenda.Add(artigoAux);

                return true;
            }
            catch (Exception e) { throw e; }
        }

        //#region ORDENACOES

        //#region MODELO
        ///// <summary>
        ///// Ordena uma lista de Artigos, por modelo, conforme a ordem desejada
        ///// </summary>
        ///// <param name="lista"></param>
        ///// <returns></returns>
        //public List<ArtigoBO> OrdenarPorModelo(ORD ordem)
        //{
        //    ArtigoBO aux; // variável intermediária para trocar posições
        //    List<ArtigoBO> listaOrdenada = new List<ArtigoBO>(); // nova lista, ordenada
        //    int tamanhoLista = artigosVenda.Count; // tamanho da lista

        //    // Para ordem Crescente
        //    #region CRESCENTE
        //    if (ordem == ORD.Cresc)
        //    {
        //        // Percorre a lista, para ir adiconando os valores
        //        for (int i = 0; i < tamanhoLista; i++)
        //        {
        //            listaOrdenada.Add(artigosVenda[i]); // Adição de um novo elemento à lista ordenada

        //            for (int j = i; j > 0; j++)
        //            {
        //                if (listaOrdenada[i].Modelo.CompareTo(listaOrdenada[i - 1].Modelo) < 0) // Verifica se o elemento adicionado é alfabeticamente menor ao anterior
        //                {
        //                    // Troca da ordem de elementos 

        //                    aux = listaOrdenada[i];

        //                    listaOrdenada[i] = listaOrdenada[i - 1];
        //                    listaOrdenada[i - 1] = aux;
        //                }

        //            }

        //        }
        //    }
        //    #endregion

        //    // Para ordem Decrescente
        //    #region DECRESCENTE
        //    else
        //    {
        //        // Percorre a lista, para ir adiconando os valores
        //        for (int i = 0; i < tamanhoLista; i++)
        //        {
        //            listaOrdenada.Add(lista[i]); // Adição de um novo elemento à lista ordenada

        //            for (int j = i; j > 0; j++)
        //            {
        //                if (listaOrdenada[i].Modelo.CompareTo(listaOrdenada[i - 1].Modelo) > 0) // Verifica se o elemento adicionado é alfabeticamente maior ao anterior
        //                {
        //                    // Troca da ordem de elementos 

        //                    aux = listaOrdenada[i];

        //                    listaOrdenada[i] = listaOrdenada[i - 1];
        //                    listaOrdenada[i - 1] = aux;
        //                }

        //            }

        //        }
        //    }
        //    #endregion

        //    return listaOrdenada;
        //}

        ///// <summary>
        ///// Ordenação standard, por modelo - faz de forma crescente
        ///// </summary>
        ///// <param name="lista"></param>
        ///// <returns></returns>
        //public List<ArtigoBO> OrdenarPorModelo()
        //{
        //    return OrdenarPorModelo(ORD.Cresc);
        //}
        //#endregion

        //#region PRECO
        ///// <summary>
        ///// Ordena uma lista de Artigos, por preço, conforme a ordem desejada
        ///// </summary>
        ///// <param name="lista"></param>
        ///// <param name="ordem"></param>
        ///// <returns></returns>
        //public List<ArtigoBO> OrdenarPorPreco(List<ArtigoBO> lista, ORD ordem)
        //{
        //    ArtigoBO aux; // variável intermediária para trocar posições
        //    List<ArtigoBO> listaOrdenada = new List<ArtigoBO>(); // nova lista, ordenada
        //    int tamanhoLista = lista.Count; // tamanho da lista

        //    // Para ordem Crescente
        //    #region CRESCENTE
        //    if (ordem == ORD.Cresc)
        //    {
        //        // Percorre a lista, para ir adiconando os valores
        //        for (int i = 0; i < tamanhoLista; i++)
        //        {
        //            listaOrdenada.Add(lista[i]); // Adição de um novo elemento à lista ordenada

        //            for (int j = i; j > 0; j++)
        //            {
        //                if (listaOrdenada[i].Preco.CompareTo(listaOrdenada[i - 1].Preco) < 0) // Verifica se o elemento adicionado tem um preço menor que o anterior
        //                {
        //                    // Troca da ordem de elementos 

        //                    aux = listaOrdenada[i];

        //                    listaOrdenada[i] = listaOrdenada[i - 1];
        //                    listaOrdenada[i - 1] = aux;
        //                }

        //            }

        //        }
        //    }
        //    #endregion

        //    // Para ordem Decrescente
        //    #region DECRESCENTE
        //    else
        //    {
        //        // Percorre a lista, para ir adiconando os valores
        //        for (int i = 0; i < tamanhoLista; i++)
        //        {
        //            listaOrdenada.Add(lista[i]); // Adição de um novo elemento à lista ordenada

        //            for (int j = i; j > 0; j++)
        //            {
        //                if (listaOrdenada[i].Modelo.CompareTo(listaOrdenada[i - 1].Modelo) > 0) // Verifica se o elemento adicionado tem um preço menor que o anterior
        //                {
        //                    // Troca da ordem de elementos 

        //                    aux = listaOrdenada[i];

        //                    listaOrdenada[i] = listaOrdenada[i - 1];
        //                    listaOrdenada[i - 1] = aux;
        //                }

        //            }

        //        }
        //    }
        //    #endregion

        //    return listaOrdenada;
        //}

        ///// <summary>
        ///// Ordenação standard, por preço - faz de forma crescente
        ///// </summary>
        ///// <param name="lista"></param>
        ///// <returns></returns>
        //public List<ArtigoBO> OrdenarPorPreco(List<ArtigoBO> lista)
        //{
        //    return (OrdenarPorPreco(lista, ORD.Cresc));
        //}
        //#endregion

        //#region DATALANCAMENTO
        ///// <summary>
        ///// Ordena uma lista de Artigos, por modelo, conforme a ordem desejada
        ///// </summary>
        ///// <param name="lista"></param>
        ///// <param name="ordem"></param>
        ///// <returns></returns>
        //public List<ArtigoBO> OrdenarPorDataLancamento(List<ArtigoBO> lista, ORD ordem)
        //{
        //    ArtigoBO aux; // variável intermediária para trocar posições
        //    List<ArtigoBO> listaOrdenada = new List<ArtigoBO>(); // nova lista, ordenada
        //    int tamanhoLista = lista.Count; // tamanho da lista

        //    // Para ordem Crescente
        //    #region CRESCENTE
        //    if (ordem == ORD.Cresc)
        //    {
        //        // Percorre a lista, para ir adiconando os valores
        //        for (int i = 0; i < tamanhoLista; i++)
        //        {
        //            listaOrdenada.Add(lista[i]); // Adição de um novo elemento à lista ordenada

        //            for (int j = i; j > 0; j++)
        //            {
        //                if (listaOrdenada[i].DataLancamento.CompareTo(listaOrdenada[i - 1].DataLancamento) < 0) // Verifica se o modelo, do elemento adicionado, foi lançado antes do modelo, de Artigos anterior
        //                {
        //                    // Troca da ordem de elementos 

        //                    aux = listaOrdenada[i];

        //                    listaOrdenada[i] = listaOrdenada[i - 1];
        //                    listaOrdenada[i - 1] = aux;
        //                }

        //            }

        //        }
        //    }
        //    #endregion

        //    // Para ordem Decrescente
        //    #region DECRESCENTE
        //    else
        //    {
        //        // Percorre a lista, para ir adiconando os valores
        //        for (int i = 0; i < tamanhoLista; i++)
        //        {
        //            listaOrdenada.Add(lista[i]); // Adição de um novo elemento à lista ordenada

        //            for (int j = i; j > 0; j++)
        //            {
        //                if (listaOrdenada[i].Modelo.CompareTo(listaOrdenada[i - 1].Modelo) > 0) // Verifica se o modelo, do elemento adicionado, foi lançado depois do modelo de Artigos anterior
        //                {
        //                    // Troca da ordem de elementos 

        //                    aux = listaOrdenada[i];

        //                    listaOrdenada[i] = listaOrdenada[i - 1];
        //                    listaOrdenada[i - 1] = aux;
        //                }

        //            }

        //        }
        //    }
        //    #endregion

        //    return listaOrdenada;
        //}

        ///// <summary>
        ///// Ordenação standard, por modelo - faz de forma crescente   
        ///// </summary>
        ///// <param name="lista"></param>
        ///// <returns></returns>
        //public List<ArtigoBO> OrdenarPorDataLancamento(List<ArtigoBO> lista)
        //{
        //    return OrdenarPorDataLancamento(lista, ORD.Cresc);
        //}
        //#endregion

        //#endregion


        #region GESTAO_DADOS_FICHEIROS
        /// <summary>
        /// Guarda artigos à venda, em Json
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static bool GuardaArtigosJson()
        {
            try
            {
                //File.Open(filePath, FileMode.OpenOrCreate); // Abre o ficheiro caso exista, ou cria em caso de inexistência 
                JsonSerializer jsonSerializer = new JsonSerializer();

                StreamWriter sw = new StreamWriter(filePath);
                JsonWriter jsonWriter = new JsonTextWriter(sw);

                jsonSerializer.Serialize(jsonWriter, artigosVenda);

                jsonWriter.Close();
                sw.Close();
                return true;
            }catch (Exception e) { throw e; }
        }

        /// <summary>
        /// Carrega dados da lista de artigos
        /// </summary>
        /// <returns></returns>
        public static bool CarregarArtigosJson()
        {
            JObject obj = null;
            JsonSerializer jsonSerializer = new JsonSerializer();
            if (File.Exists(filePath))
            {
                try
                {
                    StreamReader sr = new StreamReader(filePath);
                    JsonReader jsonReader = new JsonTextReader(sr);
                    obj = jsonSerializer.Deserialize(jsonReader) as JObject;

                    jsonReader.Close();
                    sr.Close();

                    artigosVenda = obj.ToObject(typeof(List<Artigo>)) as List<Artigo>;
                    return true;
                }
                catch (Exception e) { throw e; }
            }
            return false;
        }
    }
    #endregion
    public enum ORD
    {
        Cresc,
        Decres,
    }

}
