/* 
 * Nome: João Rodrigues
 * Nº aluno: 16928
 * UC: Linguagem de Programação II
 * Trabalho Prático 
 * Tema: App vendas de marca de roupa e sapatilhas
 */

using System;
using System.Collections.Generic;
using BusinessObjects;

/// <summary>
/// Gestor de dados
/// </summary>
namespace DataSource
{
    [Serializable]
    /// <summary>
    /// Classe Coleção + Listas de artigos
    /// </summary>
    public class Colecao
    {
        ColecaoBO colecaoInfos;
        List<Artigo> colecao;
    }

    [Serializable]
    /// <summary>
    /// Conjunto de coleções existentes
    /// </summary>
    public class Colecoes
    {
        List<ColecaoBO> colecoesLista;

        /// <summary>
        /// Adiciona uma coleção à lista de coleções
        /// </summary>
        /// <param name="col"></param>
        /// <returns></returns>
        public bool AddColecao(ColecaoBO col)
        {
           if (col!= null)
            {
                colecoesLista.Add(col);
                return true;
            }
            return false;
        }
    }
}
