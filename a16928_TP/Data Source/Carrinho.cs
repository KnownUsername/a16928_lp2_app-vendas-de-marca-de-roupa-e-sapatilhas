﻿/* 
 * Nome: João Rodrigues
 * Nº aluno: 16928
 * UC: Linguagem de Programação II
 * Trabalho Prático 
 * Tema: App vendas de marca de roupa e sapatilhas
 */

using BusinessObjects;
using System;
using System.Collections.Generic;

/// <summary>
/// Gestor de dados
/// </summary>
namespace DataSource
{

    /// <summary>
    /// Estado do carrinho
    /// </summary>
    public enum ESTADO
    {
        ADICIONANDO,
        PAGO
    }

    [Serializable]
    /// <summary>
    /// Armazena artigos para compra
    /// </summary>
    public class Carrinho
    {
        float subTotal, total;
        const float descontoLog = 0.10f; // desconto para utilizadores logados

        ESTADO estado;

        List<ArtigoBO> artigosComprados;

        #region PROPRIEDADES 
        public float SubTotal
        {
            get { return subTotal; }
            set { subTotal = value; }
        }

        public ESTADO Estado
        {
            get { return estado; }
            set { estado = value; }
        }

        #endregion

        #region CONSTRUTORES
        public Carrinho()
        {
            Init();
        }
        #endregion

        /// <summary>
        /// Inicializa um carrinho
        /// </summary>
        public void Init()
        {
            subTotal = 0;
            total = 0;
            estado = ESTADO.ADICIONANDO;
        }

        /// <summary>
        /// Atribui o valor total à variável
        /// </summary>
        /// <param name="logStatus"></param>
        public void SetTotal(LOG logStatus)
        {
            if (logStatus == LOG.ON) total = (1 - descontoLog) * subTotal;
            else total = subTotal;
        }

        /// <summary>
        /// Adicona um artigo ao carrinho
        /// </summary>
        /// <param name="artigo"></param>
        /// <param name="carrinho"></param>
        /// <returns></returns>
        public bool AdicionarArtigo(ArtigoBO artigo)
        {
            if (artigo != null)
            {
                artigosComprados.Add(artigo); // adição de artigo ao carrinho
                SubTotal += artigo.Preco; // Atualização do preço final do carrinho
                return true;
            }
            return false;
        }

        /// <summary>
        /// Reinicia o carrinho
        /// </summary>
        /// <param name="carrinho"></param>
        /// <returns></returns>
        public bool LimparCarrinho()
        {
            if (Estado == ESTADO.PAGO)
            {
                artigosComprados = null; // Apagar lista de artigos

                Init(); // reinicialização do carrinho
                return true;
            }
            return false;
        }
    }
}
