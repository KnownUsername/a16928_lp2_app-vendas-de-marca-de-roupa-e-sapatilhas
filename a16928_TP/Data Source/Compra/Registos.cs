﻿/* 
 * Nome: João Rodrigues
 * Nº aluno: 16928
 * UC: Linguagem de Programação II
 * Trabalho Prático 
 * Tema: App vendas de marca de roupa e sapatilhas
 */

using System;
using System.Collections.Generic;

/// <summary>
/// Gestor de dados
/// </summary>
namespace DataSource
{
    [Serializable]
    /// <summary>
    /// Guarda os dados de uma compra
    /// </summary>
    public class RegistoCompra
    {
        int idCliente;
        Carrinho compraEfetuada;
        DateTime dataCompra;

        #region CONSTRUTOR

        /// <summary>
        /// Efetua o registo de uma compra
        /// </summary>
        /// <param name="compra"></param>
        public RegistoCompra(Carrinho compra)
        {
            compraEfetuada = compra;
            dataCompra = DateTime.Today;
        }
        #endregion

        #region PROPRIEDADES

        public int IdCliente
        {
            get { return idCliente; }
            set { idCliente = value; }
        }
        #endregion

    }

    [Serializable]
    /// <summary>
    /// Guarda registos de compras de todos os clientes 
    /// </summary>
    class Registos
    {
        static List<RegistoCompra> registosCompras;

    }
}
