/* 
 * Nome: Jo�o Rodrigues
 * N� aluno: 16928
 * UC: Linguagem de Programa��o II
 * Trabalho Pr�tico 
 * Tema: App vendas de marca de roupa e sapatilhas
 */

using BusinessObjects;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

/// <summary>
/// Gestor de dados
/// </summary>
namespace DataSource
{
    [Serializable]
    /// <summary>
    /// Vers�o da classe Cliente onde s�o armazenadas estruturas de dados
    /// </summary>
    /// <DataStoraged>
    /// - Lista de Artigos (Guardados)
    /// - Lista de Registos (Registos de compras feitas)
    /// </DataStoraged>
    public class Cliente
    {
        ClienteBO clienteInfo; // cont�m informa��es sobre o Cliente
        List<Artigos> artigosGuardados; // lista personalizada do utilizador para guardar artigos de seu interesse
        List<RegistoCompra> comprasEfetuadas; // lista contentor de registos de compras

        public ClienteBO ClienteInfo
        {
            get { return clienteInfo; }
            set { clienteInfo = value; }
        }
    }
    [Serializable]

    /// <summary>
    /// Classe que armazena todos os clientes da aplica��o
    /// </summary>
    public class Clientes
    {

        static List<Cliente> clientesLista;
        static int totalClientes = 0; // n� de clientes alocados
        static string filePath = Environment.CurrentDirectory + "\\clientes.bin"; // destino dos dados sobre os clientes


        /// <summary>
        /// Adiciona um cliente a uma lista de clientes
        /// </summary>
        /// <param name="novoCliente"></param>
        /// <returns></returns>
        public static int InsereCliente(ClienteBO novoCliente)
        {
            Cliente clAux = new Cliente();
            novoCliente.NumeroCliente = totalClientes + 1; // atribui��o de um n�mero de cliente

            if (clientesLista == null)
            {
                clientesLista = new List<Cliente>();
            }

            clAux.ClienteInfo = novoCliente;
            clientesLista.Add(clAux);

            totalClientes++; // atualiza��o do n�mero total de clientes

            return novoCliente.NumeroCliente;
        }

        #region VERIFICACOES
        /// <summary>
        /// Verifica se o email est� registado
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public static bool EmailExiste(string email)
        {
            foreach (Cliente cl in clientesLista)
            {
                if (cl.ClienteInfo.Email == email) return true;
            }
            return false;
        }

        /// <summary>
        /// Verifica se a password introduzida coincide com a password do email introduzido
        /// </summary>
        /// <param name="password"></param>
        /// <returns></returns>
        public static bool PasswordCorresponde(string email, string password)
        {
            foreach (Cliente cl in clientesLista)
            {
                if (cl.ClienteInfo.Email == email)  // Quando � encontrado o email pretendido, o ciclo ter� de parar, ap�s a verifica��o da password
                {
                    // Verifica��o da password submetida
                    if (cl.ClienteInfo.Password == password) return true;
                    return false;
                }
            }
            return false;
        }
        #endregion

        /// <summary>
        /// Disponibiliza a informa��o do utilizador, ao mesmo, assim que inicia sess�o
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public static ClienteBO DisponibilizaInfoCliente(string email)
        {
            foreach (Cliente copy in clientesLista)
            {
                if (copy.ClienteInfo.Email == email)
                {
                    return copy.ClienteInfo;
                }
            }
            return null;
        }

        #region GESTAO_DADOS_FICHEIROS
        /// <summary>
        /// Carrega dados da lista de clientes
        /// </summary>
        public static bool CarregarListaClientes()
        {

            FileStream fileStream;
            BinaryFormatter bin = new BinaryFormatter();

            try
            {
                if (File.Exists(filePath))
                {
                    fileStream = File.OpenRead(filePath); // a abertura do ficheiro � feita em modo de ler apenas, pois n�o se pretende editar o ficheiro
                    if (fileStream.Length != 0) clientesLista = bin.Deserialize(fileStream) as List<Cliente>; // � feito um cast para lista de clientes no objeto formado
                    fileStream.Close();
                }
            }
            catch (Exception e)
            {
                throw e;
            }

            if (clientesLista != null) totalClientes = clientesLista.Count; // apenas se contabiliza se a lista n�o estiver vazia
            return true;
        }

        /// <summary>
        /// Guarda a lista de todos os clientes, juntamente com a sua informa��o
        /// </summary>
        public static bool GuardaClientes()
        {
            try
            {
                FileStream fileStream = File.Open(filePath, FileMode.OpenOrCreate); // Abre o ficheiro caso exista, ou cria em caso de inexist�ncia 
                BinaryFormatter bin = new BinaryFormatter();
                bin.Serialize(fileStream, clientesLista);
                fileStream.Close();
            }
            catch (Exception e)
            {
                throw e;
            }

            return true;
        }
 
        #endregion
    }
}
